document.getElementById("gen").addEventListener('click',function () {
    isGenerated = false;
    document.getElementById('gen').disabled = true;
    count = 0;
    let size = parseInt(document.getElementById("size").value);
    let tabletag = document.createElement('table');
    if(isGenerated===false) {
        for (let i = 0; i < size; i++) {
            let trtag = document.createElement('tr');
            trtag.setAttribute("name",i.toString());
            for (let j = 0; j < size; j++) {
                let tdtag = document.createElement('td');
                tdtag.classList.add('cell');
                tdtag.setAttribute("name",j.toString());
                trtag.appendChild(tdtag);
            }
            tabletag.appendChild(trtag);
        }
        document.body.appendChild(tabletag);
        isGenerated = true;
    }
    let table  = document.getElementsByTagName('table');
    let interval = setInterval(function () {
    for(let k = 0;k<1;k++) {
        if(count === 2*size-2){
            document.getElementById("gen").disabled = false;
            clearTimeout(interval);

        }
        if (count === 0) {
            table[0].childNodes[0].childNodes[0].classList.add("neighbour");
            table[0].childNodes[0].childNodes[0].classList.add("lamp");
            count++;
            break;
        }
        if(count===1){
            table[0].childNodes[0].childNodes[0].classList.remove("lamp");
            checkNeighbours(0,0);
            table[0].childNodes[0].childNodes[0].classList.remove("neighbour");

        }
        if(count>=1) {
            let nei = document.getElementsByClassName("neighbour");
            let tmparr = [];
            let len = nei.length;

            console.log("nei = " + nei);
            for (let i = 0; i < len; i++) {

                if (nei[i].classList.contains('lamp') === false) {
                    nei[i].classList.add("lamp");
                }
                else{
                    nei[i].classList.remove("lamp");
                }
            }
            tmparr.length = nei.length;
            for(let i = 0;i<tmparr.length;i++){
                tmparr[i] = nei[i];
            }

            for(let i = 0;i<len;i++){
                let currentTr = parseInt(tmparr[i].parentNode.getAttribute("name"));
                let currentTd = parseInt(tmparr[i].getAttribute("name"));
                console.log("nei[i] " + tmparr[i].parentNode.getAttribute("name")+ "," + tmparr[i].getAttribute("name"));

                checkNeighbours(currentTr,currentTd);

                tmparr[i].classList.remove("neighbour");
            }
            count++;
            break;
        }
    }
    console.log("count");
    },1000);
});
function checkNeighbours(currentTr,currentTd) {
    let table = document.getElementsByTagName('table');

    console.log("OK");

    if(table[0].childNodes[currentTr-1]!==undefined){
        table[0].childNodes[currentTr-1].childNodes[currentTd].classList.add("neighbour");

    }
    if(table[0].childNodes[currentTr].childNodes[currentTd-1]!==undefined){
        table[0].childNodes[currentTr].childNodes[currentTd-1].classList.add("neighbour");

    }
    if(table[0].childNodes[currentTr].childNodes[currentTd+1]!==undefined){
        table[0].childNodes[currentTr].childNodes[currentTd+1].classList.add("neighbour");

    }
    if(table[0].childNodes[currentTr+1]!==undefined){
        table[0].childNodes[currentTr+1].childNodes[currentTd].classList.add("neighbour");

    }

}