let
    canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    currentScore = document.getElementById('score'),
    direction = '',
    directionQueue = '',
    fps = 80,
    snake = [],
    snakeLength = 3,
    cellSize = 16,
    snakeColor = "green",
    foodX = [],
    foodY = [],
    food = {
        x: 0,
        y: 0
    },
    score = 0,
    boom = new Audio('boom.wav');
    eat = new Audio('bite.wav');
    self = new Audio('self.wav');
    powerUp = new Audio('power-up.wav');
    powerUp2 = new Audio('power-up2.wav');
    powerUp3 = new Audio('power-up3.wav');

for(let i = 0; i <= canvas.width - cellSize; i+=cellSize) {
    foodX.push(i);
    foodY.push(i);
}
let apple = document.getElementById("apple");
let pat;


canvas.style.outline = 'none';


function setBackground(color1, color2) {
    context.fillStyle = color1;
    context.strokeStyle = color2;

    context.fillRect(0, 0, canvas.height, canvas.width);

    for(let x = 0.5; x < canvas.width; x += cellSize) {
        context.moveTo(x, 0);
        context.lineTo(x, canvas.height);
    }
    for(let y = 0.5; y < canvas.height; y += cellSize) {
        context.moveTo(0, y);
        context.lineTo(canvas.width, y);
    }

    context.stroke()
}
function drawSquare(x,y,color) {
    context.fillStyle = color;
    context.fillRect(x, y, cellSize, cellSize);
}
function createApple() {
    food.x = foodX[Math.floor(Math.random()*foodX.length)];
    food.y = foodY[Math.floor(Math.random()*foodY.length)];

    for(let i = 0; i < snake.length; i++) {
        if(checkPos(food.x, food.y, snake[i].x, snake[i].y)) {
            createApple();
        }
    }
}
function drawApple() {
    pat = context.createPattern(apple,"repeat");
    drawSquare(food.x, food.y, pat);
}
function createSnake() {
    snake = [];
    for(let i = snakeLength; i > 0; i--) {
        let k = i * cellSize;
        snake.push({x: k, y:0});
    }
}
function drawSnake() {
    for(let i = 0; i < snake.length; i++) {
        drawSquare(snake[i].x, snake[i].y, snakeColor);
    }
}
function checkPos(x1,y1,x2,y2) {
    return x1 === x2 && y1 === y2;
}

function changeDirection(keycode) {
    if(keycode === 37 && direction !=='right') { directionQueue = 'left'; }
    else if(keycode === 38 && direction !== 'down') { event.preventDefault(); directionQueue = 'up'; }
    else if(keycode === 39 && direction !== 'left') { directionQueue = 'right'; }
    else if(keycode === 40 && direction !== 'top') {event.preventDefault(); directionQueue = 'down' }
}
function move() {
    let x = snake[0].x;
    let y = snake[0].y;
    direction = directionQueue;
    if(direction === 'right') {
        x+=cellSize;
    }
    else if(direction === 'left') {
        x-=cellSize;
    }
    else if(direction === 'up') {
        y-=cellSize;
    }
    else if(direction === 'down') {
        y+=cellSize;
    }
    let tail = snake.pop();
    tail.x = x;
    tail.y = y;
    snake.unshift(tail);
}
function game(){

    let head = snake[0];

    if(head.x < 0 || head.x > canvas.width - cellSize  || head.y < 0 || head.y > canvas.height - cellSize) {
        boom.play();
        if(score!==0) {
            addRecord(score);
        }
        setBackground();
        createSnake();
        drawSnake();
        createApple();
        drawApple();
        directionQueue = 'right';

        score = 0;
        canvas.style.animationPlayState = "paused";
        canvas.style.boxShadow = "-5px 5px 10px yellow,5px -5px 10px purple";
        let scores = document.getElementsByClassName('score');
        scores[0].style.color = "black";
        fps = 80;
        clearInterval(loop);
        loop = setInterval(game, fps);

    }

    for(let i = 1; i < snake.length; i++) {
        if(head.x === snake[i].x && head.y === snake[i].y) {
            self.play();
            if(score!==0){
                addRecord(score);
            }
            setBackground();
            createSnake();
            drawSnake();
            createApple();
            drawApple();
            directionQueue = 'right';
            score = 0;
            canvas.style.animationPlayState = "paused";
            canvas.style.boxShadow = "-5px 5px 10px yellow,5px -5px 10px purple";

            let scores = document.getElementsByClassName('score');
            scores[0].style.color = "black";
            fps = 80;
            clearInterval(loop);
            loop = setInterval(game, fps);

        }
    }

    if(checkPos(head.x, head.y, food.x, food.y)) {
        let scores = document.getElementsByClassName('score');
        snake[snake.length] = {x: head.x, y: head.y};
        createApple();
        drawApple();
        eat.play();
        score += 20;



        if(score%100===0){
            fps-=15;
            clearInterval(loop);
            loop = setInterval(game, fps);


        }
        if(score===100){
            powerUp.play();
           scores[0].style.color = "purple";

           canvas.style.boxShadow = "-8px 8px 10px pink,8px -8px 10px aqua";
        }
        else if(score === 200){
            powerUp2.play();
            scores[0].style.color = "orange";
            canvas.style.boxShadow = "-8px 8px 10px gray,8px -8px 10px royalblue";
        }
        else if(score === 300){
            powerUp3.play();
            scores[0].style.color = "aqua";
            canvas.style.boxShadow = "-10px 10px 10px red,10px -10px 10px palegreen";
            canvas.style.animationPlayState = "running";
        }
    }

    canvas.onkeydown = function(event) {
        event = event || window.event;
        changeDirection(event.keyCode);
    };

    context.beginPath();
    setBackground('#fff', '#eee');
    currentScore.innerHTML = score;
    drawSnake();
    drawApple();
    move();
}
function startNewGame() {
    direction = 'right';
    directionQueue = 'right';
    context.beginPath();
    createSnake();
    createApple();

    if(typeof loop !== 'undefined') {
        clearInterval(loop);
    }
    else {
        loop = setInterval(game, 80);

    }
}
let arr;
let count;
function init() {
    arr = new Array(100);
    count = 0;
}
init();
function addRecord(text){
    let select = document.getElementById("records");
    let value = "1";
    let tmp;
    let tmplen;

    arr[count] = text;
    count++;
    arr.sort(function (a,b) {
        return b-a;
    });
    tmplen = select.options.length;
    for(let i = 0;i<=tmplen;i++){
        tmp = (i+1).toString()+"."+arr[i].toString();
        console.log(tmp,typeof (tmp));
        select.options[i] = new Option(tmp, value);
    }
    console.log(arr);
}
document.getElementById("start").addEventListener('click',function () {
    canvas.focus();
    startNewGame();
});


