function createStartElements(){
    let div1 = document.createElement("div");
    let div2 = document.createElement("div");
    let div3 = document.createElement("div");
    let label1 = document.createTextNode('Количество клеток по ширине: ');
    let label2 = document.createTextNode('Количество клеток по высоте: ');
    let label3 = document.createTextNode('Количество мин на игровом поле: ');

    let inputWidth = document.createElement('input');
    let inputHeight = document.createElement('input');
    let inputMines = document.createElement('input');
    let start = document.createElement('input');
    let restart = document.createElement('input');

    inputWidth.setAttribute("id","width");
    inputWidth.setAttribute("type","number");
    inputWidth.setAttribute("min","1");
    inputHeight.setAttribute("id","height");
    inputHeight.setAttribute("type","number");
    inputHeight.setAttribute("min","1");
    inputMines.setAttribute("id","minesCount");
    inputMines.setAttribute("type","number");
    inputMines.setAttribute("min","1");
    start.setAttribute("id","start");
    start.setAttribute("value","Начать игру");
    start.setAttribute("type","button");
    restart.setAttribute("id","restart");
    restart.setAttribute("value","Заново");
    restart.setAttribute("type","button");
    document.body.appendChild(div1);
    div1.appendChild(label1);
    div1.appendChild(inputWidth);
    document.body.appendChild(div2);
    div2.appendChild(label2);
    div2.appendChild(inputHeight);
    document.body.appendChild(div3);
    div3.appendChild(label3);
    div3.appendChild(inputMines);
    document.body.appendChild(start);
    document.body.appendChild(restart);
}

createStartElements();
let buttonStart = document.getElementById("start");
let buttonRestart = document.getElementById("restart");
let widthInput = document.getElementById("width");
let heightInput = document.getElementById("height");
let minesInput = document.getElementById("minesCount");
let rightFlags = 0;
let minesAround = 0;


let turnsCount = 0;
let pathes = document.getElementsByClassName("path");

widthInput.value = 1;
heightInput.value = 1;
minesInput.value = 1;
buttonRestart.style.display = "none";
let isLose = false;
let isWin = false;
let useDepthCount = 0;
buttonStart.addEventListener('click', function () {
    if(parseInt(widthInput.value) < 1){
        widthInput.value = 1;
    }
    if(parseInt(heightInput.value) < 1){
        heightInput.value = 1;
    }
    if(parseInt(minesInput.value) < 1){
        minesInput.value = 1;
    }

    let width = parseInt(widthInput.value);
    let height = parseInt(heightInput.value);
    let minesCount = parseInt(minesInput.value);

    if(minesCount<width*height && width*height>=2) {

        minesInput.style.borderColor = defaultStatus;
        widthInput.style.borderColor = defaultStatus;
        heightInput.style.borderColor = defaultStatus;
        buttonRestart.style.display = "inline-block";
        buttonStart.disabled = true;
        widthInput.disabled = true;
        heightInput.disabled = true;
        minesInput.disabled = true;
        generateTable(height, width);


    }
    else if(minesCount>=width*height){
        minesInput.style.borderColor = "red";
    }
    else{
        widthInput.style.borderColor = "red";
        heightInput.style.borderColor = "red";
    }
});
buttonRestart.addEventListener('click',restartGame);
function restartGame() {
    deleteTable();
    buttonRestart.style.display = "none";
    buttonStart.disabled = false;
    widthInput.disabled = false;
    heightInput.disabled = false;
    minesInput.disabled = false;
    isLose = false;
    isWin = false;
    rightFlags = 0;
    turnsCount = 0;
}
function generateTable(rows, cols) {
    let tableTag = document.createElement('table');
    tableTag.classList.add('table');
    for(let i = 0; i < rows; i++)
    {
        let trTag = document.createElement('tr');
        for(let j = 0; j < cols; j++)
        {
            let tdTag = document.createElement('td');
            tdTag.setAttribute('name',j.toString());
            trTag.appendChild(tdTag);


        }
        trTag.setAttribute('name',i.toString());
        tableTag.appendChild(trTag);

    }
    document.body.appendChild(tableTag);

    tableTag.addEventListener('click', function(event)
    {


        let width = parseInt(widthInput.value);
        let height = parseInt(heightInput.value);
        let minesCount = parseInt(minesInput.value);

        if(isLose!==true && isWin!==true) {

            if(event.target.classList.contains("flag")===false && event.target.getAttribute("name")!==null) {
                turnsCount++;
                event.target.classList.add('selected');

                if (turnsCount === 1) {
                    let currentTr = parseInt(event.target.parentNode.getAttribute("name"));
                    let currentTd = parseInt(event.target.getAttribute("name"));
                    addMines(minesCount, height, width,currentTr,currentTd);
                    addClassesToCells();
                }
                let currentTr = parseInt(event.target.parentNode.getAttribute('name'));
                let currentTd = parseInt(event.target.getAttribute('name'));

                if (event.target.classList.contains('mine')) {
                    event.target.classList.add('mine-detonate');
                    let tdTag = document.getElementsByTagName('td');
                    for (let i = 0; i < tdTag.length; i++) {
                        if (tdTag[i].classList.contains('mine')) {
                            tdTag[i].classList.add('selected');
                        }
                    }
                    isLose = true;
                    event.target.classList.add('you_lose');
                }

                else {
                    if (turnsCount > 0) {
                        let tabTags = document.getElementsByTagName('table');


                        if (tabTags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("clear") === true &&
                            tabTags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("mine") === false) {

                            depthGo(currentTr, currentTd);
                            useDepth();

                        }
                        else {
                            setNumber(currentTr, currentTd);
                        }
                       if(checkAllCells()===true){
                           isWin=true;
                       }
                        if(isWin === true){
                            event.target.classList.add('you_win');
                        }
                    }

                }
            }
        }
        else{
            event.target.classList.add('you_win');
            event.preventDefault();
        }
    });
    let flags = parseInt(minesInput.value);
    tableTag.addEventListener('contextmenu',function (event) {
        let minesCount = parseInt(minesInput.value);


        event.preventDefault();
        if(isWin!==true && isLose!==true && flags>=0) {


            if(event.target.classList.contains('flag')){
                event.target.classList.remove('flag');
                flags++;
            }
            else{
                if(flags>0 && event.target.classList.contains('selected')===false) {
                    event.target.classList.add('flag');
                    flags--;
                }
            }



            if (event.target.classList.contains('flag') && event.target.classList.contains('mine')) {
                rightFlags++;

                if (rightFlags === minesCount && checkAllCells()===true) {
                    isWin = true;
                }
                if(isWin === true){
                    event.target.classList.add('you_win');
                }
            }
            else if(event.target.classList.contains('mine')){
                rightFlags--;
            }


        }
        else if(isWin===true){
            event.target.classList.add('you_win');
        }
    })
}
function deleteTable() {
    let tableTag = document.getElementsByTagName("table");
     if(tableTag[0]!== undefined) {
         tableTag[0].parentNode.removeChild(tableTag[0]);
     }
}
function addMines(minesCount,trCount,tdCount,currentTr,currentTd) {
    tableTags = document.getElementsByTagName("table");
    let rnd1 = 0;
    let rnd2 = 0;
    for(let i = 1;i<=minesCount;i++){
        rnd1 = Math.floor((Math.random()) * Math.floor(trCount));
        rnd2 = Math.floor((Math.random()) * Math.floor(tdCount));

            while((rnd1===currentTr && rnd2 === currentTd) || tableTags[0].childNodes[rnd1].childNodes[rnd2].classList.contains("mine") === true) {
                rnd1 = Math.floor((Math.random()) * Math.floor(trCount));
                rnd2 = Math.floor((Math.random()) * Math.floor(tdCount));

            }

            tableTags[0].childNodes[rnd1].childNodes[rnd2].classList.add("mine");
    }






}
function addClassesToCells() {
    let tableTags = document.getElementsByTagName('table');
    let width = parseInt(widthInput.value);
    let height = parseInt(heightInput.value);
    for(let i = 0;i<height;i++){
        for(let j = 0;j<width;j++){
            if(tableTags[0].childNodes[i].childNodes[j].classList.contains('mine')===false) {
                if (noMinesCheck(i, j) === true) {
                tableTags[0].childNodes[i].childNodes[j].classList.add('clear');

                }
                else {
                tableTags[0].childNodes[i].childNodes[j].classList.add(minesCheck(i, j));

                }
            }
        }
    }
    return 1;
}
function depthGo(currentTr,currentTd){

    let tabTags = document.getElementsByTagName('table');
    let width = parseInt(widthInput.value);
    let height = parseInt(heightInput.value);

    if(tabTags[0].childNodes[currentTr].childNodes[currentTd].classList.contains('clear')) {
            for (let i = currentTr; i >= 0; i--) {

                    if (tabTags[0].childNodes[i].childNodes[currentTd].classList.contains('clear')) {
                        tabTags[0].childNodes[i].childNodes[currentTd].classList.add('selected');
                        tabTags[0].childNodes[i].childNodes[currentTd].classList.add('path');

                    }
                    else {

                        setNumber(i, currentTd);

                         if(currentTd!==0 && tabTags[0].childNodes[i].childNodes[currentTd-1].classList.contains('clear')===false){
                             setNumber(i,currentTd-1);
                         }
                         if(currentTd!==width-1 && tabTags[0].childNodes[i].childNodes[currentTd+1].classList.contains('clear')===false) {
                             setNumber(i, currentTd + 1);
                         }

                        break;
                    }

            }
            for (let i = currentTr; i <= height-1; i++) {


                    if (tabTags[0].childNodes[i].childNodes[currentTd].classList.contains('clear')) {
                        tabTags[0].childNodes[i].childNodes[currentTd].classList.add('selected');
                        tabTags[0].childNodes[i].childNodes[currentTd].classList.add('path');


                    }

                    else {
                        setNumber(i, currentTd);
                        tabTags[0].childNodes[i].childNodes[currentTd].classList.add('bot-end');
                        if(currentTd!==0 && tabTags[0].childNodes[i].childNodes[currentTd-1].classList.contains('clear') === false){
                            setNumber(i,currentTd-1);
                        }
                        if(currentTd!==width-1 && tabTags[0].childNodes[i].childNodes[currentTd+1].classList.contains('clear') === false) {
                            setNumber(i, currentTd + 1);
                        }

                        break;
                    }

            }
            for (let i = currentTd; i <= width-1; i++) {

                if (tabTags[0].childNodes[currentTr].childNodes[i].classList.contains('clear')) {
                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('selected');
                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('path');

                }
                else {
                    setNumber(currentTr, i);
                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('right-end');
                    if(currentTr!==0 && (tabTags[0].childNodes[currentTr-1].childNodes[i].classList.contains('clear'))===false){
                        setNumber(currentTr-1,i);
                    }
                    if(currentTr!==height-1 && (tabTags[0].childNodes[currentTr+1].childNodes[i].classList.contains('clear')) === false){
                        setNumber(currentTr+1,i);
                    }

                    break;
                }
            }
            for (let i = currentTd; i >= 0; i--) {

                if (tabTags[0].childNodes[currentTr].childNodes[i].classList.contains('clear')) {
                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('selected');
                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('path');

                }
                else {

                    setNumber(currentTr, i);

                    tabTags[0].childNodes[currentTr].childNodes[i].classList.add('left-end');
                    if(currentTr!==0 && (tabTags[0].childNodes[currentTr-1].childNodes[i].classList.contains('clear')===false)){
                        setNumber(currentTr-1,i);
                    }
                    if(currentTr!==height-1 && (tabTags[0].childNodes[currentTr+1].childNodes[i].classList.contains('clear')===false)){
                        setNumber(currentTr+1,i);
                    }

                    break;
                }

            }
    }
}
function useDepth(){

    let iterations = 0;



        for (let i = 0; i < pathes.length; i++) {

            if (pathes[i].classList.contains("used") === false) {
                iterations++;
                pathes[i].classList.add("used");

                depthGo(parseInt(pathes[i].parentNode.getAttribute("name")),parseInt( pathes[i].getAttribute("name")));



            }
        }


    if(iterations!==0) {
        useDepth();
        useDepthCount++;

    }
}
function setNumber(currentTr,currentTd) {
    let tabtags = document.getElementsByTagName('table');
    if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("1")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('one');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("2")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('two');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("3")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('three');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("4")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('four');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("5")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('five');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("6")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('six');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("7")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('seven');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if (tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("8")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('eight');
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add('selected');
    }
    else if(tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.contains("clear")){
        tabtags[0].childNodes[currentTr].childNodes[currentTd].classList.add("selected");
        return false;
    }
}
function noMinesCheck(currentTr,currentTd) {
    let width = parseInt(widthInput.value);
    let height = parseInt(heightInput.value);
    let tableTags = document.getElementsByTagName('table');
    if(currentTd !== 0 && currentTr !==0 && currentTr !==height-1 && currentTd!==width-1){
        if( tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd+1].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd !== 0 && currentTd !== width-1 && currentTr===0){
        if( tableTags[0].childNodes[currentTr+1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false)
            {
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd !== 0 && currentTd !== width-1 && currentTr===0){
        if( tableTags[0].childNodes[currentTr+1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTr !==0 && currentTr !== height-1 && currentTd===0){
        if( tableTags[0].childNodes[currentTr-1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false ){
    return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd !==0 && currentTd !== width-1 && currentTr===height-1){
        if( tableTags[0].childNodes[currentTr-1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false)
        {
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTr !==0 && currentTr !== height-1 && currentTd===width-1){
        if( tableTags[0].childNodes[currentTr-1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd ===0 && currentTr === 0){
        if( tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd ===0 && currentTr === height-1){
        if( tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd+1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr].childNodes[currentTd+1].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd ===width-1 && currentTr === height-1 ){
        if( tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr-1].childNodes[currentTd].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else if(currentTd ===width-1 && currentTr === 0){
        if( tableTags[0].childNodes[currentTr].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd-1].classList.contains('mine')===false &&
            tableTags[0].childNodes[currentTr+1].childNodes[currentTd].classList.contains('mine')===false){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}
function minesCheck(currentTr,currentTd) {
    let width = parseInt(widthInput.value);
    let height = parseInt(heightInput.value);
    let tableTags = document.getElementsByTagName('table');
    minesAround = 0;

        if (currentTd !== 0 && currentTr !== 0 && currentTr !== height - 1 && currentTd !== width - 1) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd !== 0 && currentTd !== width - 1 && currentTr === 0) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd !== 0 && currentTd !== width - 1 && currentTr === 0) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTr !== 0 && currentTr !== height - 1 && currentTd === 0) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd !== 0 && currentTd !== width - 1 && currentTr === height - 1) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTr !== 0 && currentTr !== height - 1 && currentTd === width - 1) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd === 0 && currentTr === 0) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd === 0 && currentTr === height - 1) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd + 1].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd === width - 1 && currentTr === height - 1) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr - 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;

        }
        else if (currentTd === width - 1 && currentTr === 0) {
            minesAround = 0;
            if (tableTags[0].childNodes[currentTr].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd - 1].classList.contains('mine') === true) minesAround++;
            if (tableTags[0].childNodes[currentTr + 1].childNodes[currentTd].classList.contains('mine') === true) minesAround++;


    }
        return minesAround;

}
function checkAllCells() {
    let tdTags = document.getElementsByTagName('td');
    let minesCount = parseInt(minesInput.value);
    let selectCount = 0;
    for(let i = 0;i<tdTags.length;i++){
        if(tdTags[i].classList.contains('flag')===false) {
            if (tdTags[i].classList.contains('selected')) {
                selectCount++;
            }
            else {
                break;
            }
        }

    }
    console.log("Select count = " + selectCount);
    console.log(tdTags.length - minesCount);
    console.log(selectCount === tdTags.length - minesCount);
    return selectCount === tdTags.length - minesCount;
}