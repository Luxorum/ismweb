function Car(obj){
    this.make = obj.make;
    this.model = obj.model;
    this.fuel = obj.fuel;
    this.started = obj.started;
    this.start = function () {
        if(this.fuel>0) {
            this.started = true;
            console.log("Машина " + this.make + " " + this.model + " завелась");
        }
        else{
            console.log("У машины " + this.make + " " + this.model + " нет топлива,поэтому ее невозможно завести");
        }
    };
    this.stop = function () {
      this.started = false;
        console.log("Машина " +this.make + " " + this.model +" остановлена");
    };
    this.drive = function () {
      if(this.fuel>0 && this.started===true){
          this.fuel--;
          if(this.fuel>=0) {
              console.log("Машина " + this.make + " " + this.model + " проехала единицу расстояния,теперь количество топлива = " + this.fuel);
          }
          if(this.fuel===0){
              console.log("У машины " + this.make + " " + this.model + " кончилось топливо и теперь она будет остановлена! ");
          }
      }
      else if(this.fuel===0 && this.started===true){
          this.started=false;
          console.log("Машина " +this.make + " " + this.model +" остановлена,так как в ней нет топлива");
      }
      else if(this.started===false && this.fuel>0){
          console.log("Машина " +this.make + " " + this.model +" не включена");
      }
    };
    this.addFuel = function (amount) {
      this.fuel+=amount;
        console.log("Машина " +this.make + " " + this.model +" теперь имеет " + this.fuel + " топлива");
    };


}

    let cars = [
        {make: "Fiat", model: "500", fuel: 0, started: false},
        {make: "GM", model: "Cadillac", fuel: 50, started: false},
        {make: "Webwille Motors", model: "Taxi", fuel: 10, started: true},
    ];

    let fiat = new Car(cars[0]);
    let cadi = new Car(cars[1]);
    let taxi = new Car(cars[2]);

    document.getElementById("start").addEventListener('click', function () {

        let index = document.getElementById('list').options.selectedIndex;
        if (index === 0) {
            fiat.start();
        }
        else if (index === 1) {
            cadi.start();
        }
        else {
            taxi.start();
        }
    });
    document.getElementById("stop").addEventListener('click', function () {

        let index = document.getElementById('list').options.selectedIndex;
        if (index === 0) {
            fiat.stop();
        }
        else if (index === 1) {
            cadi.stop();
        }
        else {
            taxi.stop();
        }
    });
    document.getElementById("drive").addEventListener('click', function () {

        let index = document.getElementById('list').options.selectedIndex;
        if (index === 0) {
            fiat.drive();
        }
        else if (index === 1) {
            cadi.drive();
        }
        else {
            taxi.drive();
        }
    });
    document.getElementById("addfuel").addEventListener('click', function () {

        let index = document.getElementById('list').options.selectedIndex;
        let fuelamount = parseInt(document.getElementById("textadd").value);
        if (index === 0) {
            fiat.addFuel(fuelamount);
        }
        else if (index === 1) {
            cadi.addFuel(fuelamount);
        }
        else {
            taxi.addFuel(fuelamount);
        }
    });
document.getElementById("info").addEventListener('click', function () {

    let index = document.getElementById('list').options.selectedIndex;

    if (index === 0) {
        console.log(fiat);
    }
    else if (index === 1) {
        console.log(cadi);
    }
    else {
        console.log(taxi);
    }
});
