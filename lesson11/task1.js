


function Cat(name,weight){
    this.name = name;
    this.weight = weight;
    this.say = function() {
        if(this.weight < 13) {
            return this.name + " says Miu";
        }
        else {
            return this.name + " says Meow";
        }
    }
}


function Dog(name,weight){
    this.name = name;
    this.weight = weight;
    this.say = function() {
        if(this.weight < 25) {
            return this.name + " says Yip!";
        }
        else {
            return this.name + " says Woof!";
        }
    }
}

let animals = [
    {class: Cat,args:{name: "Bars",weight: 15}},
    {class: Dog,args:{name: "Spot",weight: 25}},
    {class: Cat,args:{name: "Murzyk",weight: 12}},
    {class: Cat,args:{name: "Whiskers",weight: 15}},
    {class: Dog,args:{name: "Fluffy",weight: 15}},
    {class: Dog,args:{name: "Fido",weight: 15}},

];

for(let i = 0;i<animals.length;i++){
    if(animals[i].class === Cat){
        animals[i] = new Cat(animals[i].args.name,animals[i].args.weight);
        console.log(animals[i]);
        console.log(animals[i].say());
    }
    else{
        animals[i] = new Dog(animals[i].args.name,animals[i].args.weight);
        console.log(animals[i]);
        console.log(animals[i].say());
    }

}




